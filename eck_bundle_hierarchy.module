<?php
/**
 * @file
 * ECK bundle hierarchy module.
 */
 
/**
 * Implements hook_menu().
 */
function eck_bundle_hierarchy_menu() {
  
  module_load_include('inc', 'eck', 'eck.classes');
  $path = eck__entity_type__path();

  foreach (EntityType::loadAll() as $entity_type) {
    $items["{$path}/{$entity_type->name}/list_tree"] = array(
      'title' => 'Bundle List Tree',
      'description' => "View all the bundles for '{$entity_type->label}'",
      'page callback' => "eck_bundle_hierarchy_bundle_list_tree_page",
      'page arguments' => array(3),
      'access callback' => 'eck__multiple_access_check',
      'access arguments' => array( array('eck administer entity types',
                                         'eck add entity types',
                                         'eck edit entity types',
                                         'eck administer bundles', 
                                         'eck list bundles', 
                                         "eck administer {$entity_type->name} bundles",
                                         "eck list {$entity_type->name} bundles"
                                         ) ),
      'file' => 'eck_bundle_hierarchy.pages.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 101,
    );
  }
  
  return $items;
}

/**
 * Implements hook_form_FORMID_alter().
 */
function eck_bundle_hierarchy_form_eck__bundle__add_alter(&$form, &$form_state) {
  eck_bundle_hierarchy_bundle_form($form, $form_state);
}

/**
 * Implements hook_form_FORMID_alter().
 */
function eck_bundle_hierarchy_form_eck__bundle__edit_form_alter(&$form, &$form_state) {
  eck_bundle_hierarchy_bundle_form($form, $form_state);
}

/**
 * Implements hook_form_FORMID_alter().
 */
function eck_bundle_hierarchy_form_eck__bundle__delete_form_alter(&$form, &$form_state) {
  $entity_type_name = isset($form['entity_type']['#value']->name) ? $form['entity_type']['#value']->name : NULL;
  $bundle_name = isset($form['bundle']['#value']->name) ? $form['bundle']['#value']->name : NULL;
  
  // Delete database record
  if (!empty($entity_type_name) && !empty($bundle_name)) {
    
    /** @TODO: Verify for child elements and replace their parent **/     
    
    db_delete('eck_bundle_hierarchy')
      ->condition('entity_type', $entity_type_name)
      ->condition('bundle', $bundle_name)
      ->execute();
    
  }
}

/**
 * Bundle form
 */
function eck_bundle_hierarchy_bundle_form(&$form, &$form_state) {

  $entity_type_name = isset($form['entity_type']['#value']->name) ? $form['entity_type']['#value']->name : NULL;
  $bundle_name = isset($form['bundle']['#value']->name) ? $form['bundle']['#value']->name : NULL;
  
  $bundle_info = eck_bundle_hierarchy_get_bundle_info($entity_type_name, $bundle_name);
  
  $parent_default_value = 0;
  if (!empty($bundle_info['parent_entity_type'])) {
    $parent_default_value = $bundle_info['parent_entity_type'] . '||' . $bundle_info['parent_bundle'];
  }
  
  $form['eck_bh_parent_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => 'ECK Bundle Hierarchy',
  );
  
  $tree = eck_bundle_hierarchy_get_bundles_tree();
  $bundles_options = eck_bundle_hierarchy_get_bundles_tree_options($tree);
  
  $options = array(0 => '<' . t('root') . '>') + $bundles_options;
  
  // Remove current bundle if bundle is not new
  if (isset($form['bundle']['#value']->is_new) && $form['bundle']['#value']->is_new == FALSE) {
    unset($options[$entity_type_name . '||' . $bundle_name]);
  }
  
  $form['eck_bh_parent_wrapper']['eck_bh_parent'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => 'Parent',  
    '#description' => 'Select the parent bundle to add this bundle as a child.',
    '#default_value' => $parent_default_value,
  );
  
  $form['eck_bh_parent_wrapper']['eck_bh_weigth'] = array(
    '#type' => 'textfield',
    '#size' => 6,
    '#title' => 'Weight',
    '#default_value' => isset($bundle_info['weight']) ? $bundle_info['weight'] : 0,
  );  
  
  $form['#submit'][] = 'eck_bundle_hierarchy_bundle_form_submit';
}

function eck_bundle_hierarchy_bundle_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  
  if (!isset($form['bundle'])) {
    $bundle_name = $values['bundle_name'];
  }
  else {
    $bundle_name = $form['bundle']['#value']->name;
  }
  
  if ($values['eck_bh_parent'] <> '0') {
    $parents = explode('||', $values['eck_bh_parent']);
    $parent_entity_type = $parents[0];
    $parent_bundle = $parents[1];
  }
  else {
    $parent_entity_type = NULL;
    $parent_bundle = NULL;
  }
  
  $primary_keys = array();
  
  if (isset($form['bundle']['#value']->is_new) && $form['bundle']['#value']->is_new == FALSE) {
    $primary_keys = array('bundle', 'entity_type');  
  }
  
  $record = array(
    'bundle' => $bundle_name,
    'entity_type' => $form['entity_type']['#value']->name,
    'parent_bundle' => $parent_bundle,
    'parent_entity_type' => $parent_entity_type,
    'weight' => $values['eck_bh_weigth'],
  );

  drupal_write_record('eck_bundle_hierarchy', $record, $primary_keys);
    
}

function eck_bundle_hierarchy_get_bundle_info($entity_type_name, $bundle_name) {
  
  if (empty($entity_type_name) || empty($bundle_name)) {
    return array();
  }
  
  $result = db_select('eck_bundle_hierarchy', 'b')
              ->fields('b')
              ->condition('bundle', $bundle_name, '=')
              ->condition('entity_type', $entity_type_name, '=')
              ->orderBy('b.weight')
              ->orderBy('b.bundle')
              ->execute()
              ->fetchAssoc();
              
  return $result;
}

function eck_bundle_hierarchy_get_bundles_tree($entity_type = NULL, $bundle = NULL) {
  module_load_include('inc', 'eck', 'eck.bundle');
    
  $query = db_select('eck_bundle_hierarchy', 'b')->fields('b');
              
  $query->orderBy('b.weight')
          ->orderBy('b.bundle');
  
  if (!empty($entity_type)) {
    $query->condition('entity_type', $entity_type, '=');
  }
  
  if (!empty($bundle)) {
    $query->condition('bundle', $bundle, '=');
  }
          
  $result = $query->execute();
              
  $bundles = array();  
  foreach($result as $bundle) {
    $bundle_info = Bundle::loadByMachineName($bundle->entity_type . '_' . $bundle->bundle);
    $parent_name = isset($bundle->parent_entity_type) ? $bundle->parent_entity_type . '||' . $bundle->parent_bundle : 0;
    $bundles[$parent_name][$bundle->entity_type . '||' . $bundle->bundle] = array(      
      'title' => $bundle_info->label,
      'weight' => $bundle->weight,
      'entity_type' => $bundle->entity_type,
      'bundle' => $bundle->bundle,
    );
  }
    
  return $bundles;
}

function eck_bundle_hierarchy_get_bundles_tree_options($bundles) {
  $tree = _eck_bundle_hierarchy_get_bundles_tree_hierarchy($bundles);
  $options = array();
  _eck_bundle_hierarchy_get_bundles_tree_options_hierarchy($tree, 0, $options);
  return $options;
}

function _eck_bundle_hierarchy_get_bundles_tree_options_hierarchy($bundles, $level = 0, &$options = array()) {
  foreach($bundles as $bundle_key => $bundle) {
    $options[$bundle_key] = str_repeat('- ', $level) . $bundle['title'];
    if(isset($bundle['children']) && (count($bundle['children']) > 0)) {
      _eck_bundle_hierarchy_get_bundles_tree_options_hierarchy($bundle['children'], $level + 1, $options);
    }
  }
}

function _eck_bundle_hierarchy_get_bundles_tree_hierarchy($bundles, $level = 0, $childs = array()) {
  $tree = array();  
  if($level == 0) {
    $childs = $bundles[0];
  }  
  if(isset($childs)) {
    foreach($childs as $bundle_key => $bundle) {
      $tree[$bundle_key] = $bundle;
      if(isset($bundles[$bundle_key])) {
        $tree[$bundle_key]['children'] = _eck_bundle_hierarchy_get_bundles_tree_hierarchy($bundles, $level + 1, $bundles[$bundle_key]);
      }
    }
  }
  return $tree;
}
