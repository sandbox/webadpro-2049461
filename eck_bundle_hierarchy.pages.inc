<?php
/**
 * @file
 * Pages for ECK Bundle Hierarchy Modules.
 */
 
/**
 * Callback page to view the tree list of bundles.
 */
function eck_bundle_hierarchy_bundle_list_tree_page($entity_type) {
  
  module_load_include('inc', 'eck', 'eck.classes');  
  
  $path = eck__entity_type__path();
  $entity_type = entity_type_load($entity_type);
  
	$build['action_links'] = array(
		'#markup' => '<ul class="action-links"><li>' . l(t('Add bundle'), "{$path}/{$entity_type->name}/add") . '</li></ul>',
	);
	  
  //Check that the user has permissions to view bundle lists:
  if( eck__multiple_access_check(
      array( 'eck administer bundles', 
             'eck list bundles',
             "eck administer {$entity_type->name} bundles",
             "eck list {$entity_type->name} bundles"
  ) ) )
  {  
  
    $bundles_info = array();
    foreach (Bundle::loadByEntityType($entity_type) as $bundle) {      
      $admin_info = get_bundle_admin_info($entity_type->name, $bundle->name);
      $uri = $admin_info['path'];
      
      $allowed_operations = '';
      //Check that the user has permissions to delete:
      
      if( eck__multiple_access_check(
        array( 'eck administer bundles', 
               'eck delete bundles',
               "eck administer {$entity_type->name} bundles",
               "eck delete {$entity_type->name} bundles"
              ) )
      ) $allowed_operations[] = l(t('edit'), $uri."/edit");
      
      if( eck__multiple_access_check(
        array( 'eck administer bundles', 
               'eck delete bundles',
               "eck administer {$entity_type->name} bundles",
               "eck delete {$entity_type->name} bundles"
              ) )
      ) $allowed_operations[] = l(t('delete'), $uri."/delete");      
            
      $bundles_info[$entity_type->name . '||' . $bundle->name] = array(
        'label' => $bundle->label,
        'uri' => $uri,
        'operations' => implode(" - ", $allowed_operations),
      );
    }
    
    $header = array(t('Type'), array('data' => t('Operations'), 'colspan' => '1'));
    $rows = array();
    
    $tree = eck_bundle_hierarchy_get_bundles_tree($entity_type->name);
    $bundles = eck_bundle_hierarchy_get_bundles_tree_options($tree);
    
    foreach($bundles as $key => $bundle_label) {
      $bundle_info = $bundles_info[$key];
      $rows[] = array(
        l($bundle_label, url($bundle_info['uri'], array('absolute' => TRUE))),
        $bundle_info['operations']
      );
    }
 
    $build['bundle_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }
  
  return $build;
}
